<?php

declare(strict_types=1);

namespace DKX\SlimSecurity;

use DKX\CallableParser\CallableParser;
use DKX\CallableParser\Callables\MethodCallCallable;
use DKX\Security\Security;
use DKX\SlimSecurity\Annotations\IsGranted;
use Doctrine\Common\Annotations\AnnotationReader;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use ReflectionClass;
use Slim\Interfaces\InvocationStrategyInterface;

final class SecuredRequestResponse implements InvocationStrategyInterface
{


	/** @var \Slim\Interfaces\InvocationStrategyInterface */
	private $child;

	/** @var \DKX\Security\Security */
	private $security;

	/** @var \Doctrine\Common\Annotations\AnnotationReader */
	private $annotationReader;


	public function __construct(InvocationStrategyInterface $child, Security $security, AnnotationReader $annotationReader)
	{
		$this->child = $child;
		$this->security = $security;
		$this->annotationReader = $annotationReader;
	}


	public function __invoke(callable $callable, ServerRequestInterface $request, ResponseInterface $response, array $routeArguments)
	{
		$parsed = CallableParser::parse($callable);

		if ($parsed instanceof MethodCallCallable) {
			/** @var \DKX\SlimSecurity\Annotations\IsGranted|null $isGranted */
			$isGranted = $this->annotationReader->getClassAnnotation(
				new ReflectionClass($parsed->getObject()),
				IsGranted::class
			);

			if ($isGranted !== null) {
				$this->security->denyAccessUnlessGranted($isGranted->getGrantedFor());
			}
		}

		return $this->child->__invoke($callable, $request, $response, $routeArguments);
	}

}
