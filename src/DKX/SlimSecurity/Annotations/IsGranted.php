<?php

declare(strict_types=1);

namespace DKX\SlimSecurity\Annotations;

use Doctrine\Common\Annotations\Annotation\Target;

/**
 * @Annotation
 * @Target({"CLASS"})
 */
final class IsGranted
{


	/** @var string */
	private $grantedFor;


	public function __construct(array $values)
	{
		$this->grantedFor = $values['value'];
	}


	public function getGrantedFor(): string
	{
		return $this->grantedFor;
	}

}
