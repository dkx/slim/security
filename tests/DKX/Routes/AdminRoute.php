<?php

declare(strict_types=1);

namespace DKX\SlimSecurityTests;

use DKX\SlimSecurity\Annotations\IsGranted;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @IsGranted("ROLE_ADMIN")
 */
final class AdminRoute
{


	public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
	{
		return $response;
	}

}
