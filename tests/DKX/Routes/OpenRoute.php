<?php

declare(strict_types=1);

namespace DKX\SlimSecurityTests;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

final class OpenRoute
{


	public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
	{
		return $response;
	}

}
