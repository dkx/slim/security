<?php

declare(strict_types=1);

/** @var \Composer\Autoload\ClassLoader|bool $loader */

use Doctrine\Common\Annotations\AnnotationRegistry;

require_once __DIR__. '/../../vendor/autoload.php';

AnnotationRegistry::registerLoader('class_exists');

require_once __DIR__. '/Routes/OpenRoute.php';
require_once __DIR__. '/Routes/AdminRoute.php';
