<?php

declare(strict_types=1);

namespace DKX\SlimSecurityTests;

use DKX\Security\Exception\ForbiddenException;
use DKX\Security\Security;
use DKX\SlimSecurity\SecuredRequestResponse;
use Doctrine\Common\Annotations\AnnotationReader;
use Mockery;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Handlers\Strategies\RequestResponse;

final class SecuredRequestResponseTest extends TestCase
{


	public function testSecured_nothing(): void
	{
		self::expectNotToPerformAssertions();

		$this->callRoute(new OpenRoute);
	}


	public function testSecured_deny(): void
	{
		self::expectException(ForbiddenException::class);
		self::expectExceptionMessage('Access denied for "ROLE_ADMIN"');

		$this->callRoute(new AdminRoute);
	}


	private function callRoute(callable $route): void
	{
		$requestResponse = $this->createSecuredRequestResponse();
		$requestResponse([$route, '__invoke'], $this->createRequest(), $this->createResponse(), []);
	}


	private function createSecuredRequestResponse(): SecuredRequestResponse
	{
		return new SecuredRequestResponse(
			new RequestResponse,
			new Security,
			new AnnotationReader
		);
	}


	private function createRequest(): ServerRequestInterface
	{
		return Mockery::mock(ServerRequestInterface::class);
	}


	private function createResponse(): ResponseInterface
	{
		return Mockery::mock(ResponseInterface::class);
	}

}
