# DKX/Slim/Security

Security package inspired by symfony security and voters. Based on [dkx/security](https://gitlab.com/dkx/php/security).

Only for class routes.

See [dkx/security](https://gitlab.com/dkx/php/security) for complete documentation.

## Installation

```bash
$ composer require dkx/security dkx/slim-security
```

## Usage

```php
<?php

use DKX\Security\Security;
use DKX\SlimSecurity\SecuredRequestResponse;
use Doctrine\Common\Annotations\AnnotationReader;
use Slim\Container;

$container = new Container;
$container['foundHandler'] = function() use ($container) {
    $original = $container->get('foundHandler');
    
    return new SecuredRequestResponse(
        $original,
        new Security,
        new AnnotationReader
    );
};
```

## Annotate controllers

```php
<?php

use DKX\SlimSecurity\Annotations\IsGranted;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @IsGranted("ROLE_ADMIN")
 */
final class DetailController
{
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $response;
    }
}
```
